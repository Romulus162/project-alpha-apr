from django.contrib.auth.decorators import login_required
from .models import Task
from .forms import TaskForm
from projects.models import Project
from django.shortcuts import get_object_or_404, render, redirect

# Create your views here.


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    # tasks = Task.objects.all()
    context = {"tasks": tasks, "project": project}
    return render(request, "tasks/details.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    return render(request, "tasks/tasks.html", {"tasks": tasks})
