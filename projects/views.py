from django.shortcuts import render, redirect
from .models import Project
from .forms import ProjectForm
from django.contrib.auth.decorators import login_required
import openai, os
from dotenv import load_dotenv
load_dotenv()
# from django.urls import reverse
# Create your views here.

key = os.getenv("OPENAI_KEY", None)

@login_required
def list_projects(request):
    instances = Project.objects.filter(owner=request.user)
    # instances = Project.objects.filter(owner=request.user)
    chatbot_response = None
    if key is not None and request.method == "POST":
        openai.api_key = key
        user_input = request.POST.get("user_input")
        prompt = f"don't open up with this statement.You are a fisherman named 'Bob' who only replies in third person, unfortunately, you don't remember much after ... NAM!!!!!:{user_input}"

        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            max_tokens=256,
            temperature=0.9,

        )
        print(response)

        chatbot_response = response["choices"][0]["text"]
    context = {"instances": instances, "response": chatbot_response}

    return render(request, "projects/projects.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


def bevy_proj(request):
    return render(request, "projects/bevy.html")
