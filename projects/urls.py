from django.urls import path
from .views import list_projects, create_project, bevy_proj

urlpatterns = [
    path("", list_projects, name="list_projects"),
    # path('projects/', list_projects, name=''),
    path("create/", create_project, name="create_project"),
    path("platformer/", bevy_proj, name="bevy_proj")
]


# # I suspect a solution to the notorioiius
# #  feature 6 issue may lie in line 6 (path('projects/',
# #  list_projects, name='list_projects'),)
